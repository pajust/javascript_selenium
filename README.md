# JavaScript Selenium Automated Tests

## Building and Installing
### 1. Create a package.json file
```shell
npm init
```

### Example result:

```shell
{
  "name": "test_open_url",
  "version": "1.0.0",
  "description": "open_url_test_case",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "pajust",
  "license": "MIT",
  "keywords": [],
  "dependencies": {
    "selenium-webdriver": "^4.0.0-alpha.3"
  },
  "devDependencies": {}
}

```
### 2. Selenium may be installed via npm with
```shell
npm install selenium-webdriver
```

### 3. Install ChromeDriver via npm with
```shell
npm install chromedriver
```

## Running Test Case
### 1. Go to main diertory with TestCase
### 2. Run test with 
```shell
node index
```
## File Stucture 
### Project files structure:
```shell
 
    ├── ...
    │
    ├── lib                         # Helper methods
    │   └── baseCase.js             # Generic test functionality 
    │
    ├── test                        # Test Cases
    │   └── searchInput.test.js     # Testing searching input
    │ 
    ├── utils                       # Utility files for testing
    │   ├── config.json             # Basic configuration data
    │   └── locator.json            # CSS identifier forelements 
    │ 
    ├── training_scripts             # Testing JavaScript 
    │   ├── first_page             
    │   ├── math_js             
    │   ├── memory_card_game
    │   └── strings
    │
    ├── package.json                # Project dependency
    ├── index.js                    # Main file to run tests
    ├── ...                    
```
