const baseMudule = require('./test/searchInput.test.js');
const json = require('./utilts/config.json')

async function testSuite() 
{
   base = new baseMudule();
   driver = base.driver;

   try {
   await base.visit(json.tested_url_address);
   await base.findSearchInputByName(json.google_search_input_name);
   
   } finally {
   await base.quit();
   }
}
testSuite();