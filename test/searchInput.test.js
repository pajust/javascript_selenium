const { By } = require('selenium-webdriver');
let baseModule = require('../lib/baseCase');

let searchInput;

  baseModule.prototype.findSearchInputByName = async function (searchInputSelectorId) 
  {
   
    searchInput = await this.findBy(By.name(searchInputSelectorId));
  }

module.exports = baseModule;
