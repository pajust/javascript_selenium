const {Builder, By, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
let chromedriver = require('chromedriver');
const json = require('../utilts/config.json');

var baseModule = function() {
    this.driver = new Builder()
        .forBrowser('chrome')
        .build();

    // open url
    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };

    // quit webdriver's present session
    this.quit = async function() {
        return await this.driver.quit();
    };

    // wait and find a element by soecific LocatorStategy name for example id, name 
    this.findBy = async function(LocatorStrategy, LocatorValue) {
        await this.driver.wait(until.elementLocated(LocatorStrategy, LocatorValue), json.timeout, 'Looking for element');
        return await this.driver.findElement(LocatorStrategy, LocatorValue);
    };

    // fill input web elements
    this.write = async function (el, txt) {
        return await el.sendKeys(txt);
    };
}

module.exports = baseModule;
